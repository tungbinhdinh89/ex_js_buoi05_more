// Bài 1: Tính thuế thu nhập cá nhân
// tính thuế suất thu nhập
function thueSuatThuNhap(tongThuNhap) {
  var thueSuat;
  if (tongThuNhap <= 60000000) {
    thueSuat = 0.05;
  } else if (tongThuNhap > 60000000 && tongThuNhap <= 120000000) {
    thueSuat = 0.1;
  } else if (tongThuNhap > 120000000 && tongThuNhap <= 210000000) {
    thueSuat = 0.15;
  } else if (tongThuNhap > 210000000 && tongThuNhap <= 384000000) {
    thueSuat = 0.2;
  } else if (tongThuNhap > 384000000 && tongThuNhap <= 624000000) {
    thueSuat = 0.25;
  } else if (tongThuNhap > 624000000 && tongThuNhap <= 960000000) {
    thueSuat = 0.3;
  } else if (tongThuNhap > 960000000) {
    thueSuat = 0.35;
  }
  return thueSuat;
}

// tính tiền thuế thu nhập

function tinhTienThue() {
  var hoTen = document.getElementById('ho_ten').value;

  var tongThuNhapNam = document.getElementById('thu_nhap').value * 1;

  var nguoiPhuThuoc = document.getElementById('nguoi_phu_thuoc').value * 1;
  var result = document.getElementById('result');

  var thueThuNhap = thueSuatThuNhap(tongThuNhapNam);

  var thuNhapChiuThue = tongThuNhapNam - 4000000 - nguoiPhuThuoc * 1600000;

  var tinhThueThuNhap = thuNhapChiuThue * thueThuNhap;
  if (tinhThueThuNhap > 0) {
    result.innerHTML = `Họ tên: ${hoTen} ; Tiền thuế thu nhập cá nhân là: ${new Intl.NumberFormat(
      'de-DE',
      {
        style: 'currency',
        currency: 'VND',
      }
    ).format(tinhThueThuNhap)}`;
  } else {
    alert((result.innerHTML = `Số tiền thu nhập không hợp lệ`));
  }
}

// Bài 02: Tính tiền cáp

// show số kết nối
function showMe() {
  var khachHang = document.getElementById('loai_khach_hang').value;
  if (khachHang == 'doanhNghiep') {
    document.getElementById('ketNoi').style.display = 'block';
  } else {
    document.getElementById('ketNoi').style.display = 'none';
  }
}

// phí dịch vụ cơ bản
// function phiDichVuCoBan(soKetNoi) {
//   var phiDV;
//   if (soKetNoi < 1) {
//     return alert('Không Hợp Lệ');
//   }

//   if (soKetNoi >= 1 && soKetNoi <= 10) {
//     var phiDV = 75;
//   } else {
//     var phiDV = 5;
//   }
//   return phiDV;
// }

// tính tiền cáp
function tinhTienCap() {
  var loaiKH = document.getElementById('loai_khach_hang').value;
  console.log('loaiKH: ', loaiKH);

  var maKH = document.getElementById('ma_khach_hang').value;
  var soKenhCC = document.getElementById('so_kenh').value * 1;
  console.log('soKenhCC: ', soKenhCC);
  var soKetNoi = document.getElementById('so_ket_noi').value * 1;
  var result2 = document.getElementById('result2');
  // var phiDVCB = phiDichVuCoBan(soKetNoi);
  if (loaiKH == 'nhaDan') {
    var tinhTienCap = 4.5 + 20.5 + soKenhCC * 7.5;
    console.log('tinhTienCap: ', tinhTienCap);

    return (result2.innerHTML = `Mã Khách Hàng: ${maKH} ; Tiền cáp : ${new Intl.NumberFormat(
      'de-DE',
      {
        style: 'currency',
        currency: 'USD',
      }
    ).format(tinhTienCap)}`);
  }
  if (loaiKH == 'doanhNghiep' && soKetNoi >= 1 && soKetNoi <= 10) {
    tinhTienCap = 15 + 75 + soKenhCC * 50;
    return (result2.innerHTML = `Mã Khách Hàng: ${maKH} ; Tiền cáp : ${new Intl.NumberFormat(
      'de-DE',
      {
        style: 'currency',
        currency: 'USD',
      }
    ).format(tinhTienCap)}`);
  }
  if (loaiKH == 'doanhNghiep' && soKetNoi > 10) {
    tinhTienCap = 15 + 5 * (soKetNoi - 10) + 75 + soKenhCC * 50;
    console.log('tinhTienCap: ', tinhTienCap);
    return (result2.innerHTML = `Mã Khách Hàng: ${maKH} ; Tiền cáp : ${new Intl.NumberFormat(
      'de-DE',
      {
        style: 'currency',
        currency: 'USD',
      }
    ).format(tinhTienCap)}`);
  }
}
